USE [master]
GO
/****** Object:  Database [TalentSoftv0.1]    Script Date: 14/12/2017 17:08:32 ******/
CREATE DATABASE [TalentSoftv0.1]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TalentSoftv0.1', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\TalentSoftv0.1.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'TalentSoftv0.1_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\TalentSoftv0.1_log.ldf' , SIZE = 20096KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [TalentSoftv0.1] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TalentSoftv0.1].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TalentSoftv0.1] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET ARITHABORT OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TalentSoftv0.1] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TalentSoftv0.1] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TalentSoftv0.1] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TalentSoftv0.1] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TalentSoftv0.1] SET  MULTI_USER 
GO
ALTER DATABASE [TalentSoftv0.1] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TalentSoftv0.1] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TalentSoftv0.1] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TalentSoftv0.1] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [TalentSoftv0.1] SET DELAYED_DURABILITY = DISABLED 
GO
USE [TalentSoftv0.1]
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Applications](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Applications] PRIMARY KEY CLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Files]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Files](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[size] [int] NOT NULL,
	[mtime] [datetime] NOT NULL,
	[time] [datetime] NOT NULL CONSTRAINT [DF_Files_time]  DEFAULT (getdate()),
 CONSTRAINT [PK_Files] PRIMARY KEY CLUSTERED 
(
	[name] ASC,
	[size] ASC,
	[mtime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PERMISSION]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PERMISSION](
	[JOBCODE] [nvarchar](12) NOT NULL,
	[LEVEL] [nvarchar](50) NULL,
 CONSTRAINT [PK_JOBCODE] PRIMARY KEY CLUSTERED 
(
	[JOBCODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[pers_max_groupe2]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pers_max_groupe2](
	[EMPLID] [nvarchar](11) NOT NULL,
	[EMPL_RCD] [numeric](38, 0) NOT NULL,
	[PER_ORG] [nvarchar](3) NOT NULL,
	[ALTER_EMPLID] [nvarchar](11) NULL,
	[EFFDT] [datetime] NOT NULL,
	[WH_PAY_EMPLID] [nvarchar](11) NOT NULL,
	[LAST_NAME] [nvarchar](30) NULL,
	[FIRST_NAME] [nvarchar](30) NULL,
	[PHONE] [nvarchar](24) NULL,
	[PHONE1] [nvarchar](24) NULL,
	[EMAIL_ADDR] [nvarchar](70) NULL,
	[HR_STATUS] [nvarchar](1) NOT NULL,
	[BUSINESS_UNIT] [nvarchar](5) NOT NULL,
	[COMPANY] [nvarchar](3) NOT NULL,
	[CMPNY_SENIORITY_DT] [datetime] NULL,
	[LEAVE_DT] [datetime] NULL,
	[ACTION] [nvarchar](3) NOT NULL,
	[ACTION_REASON] [nvarchar](3) NOT NULL,
	[DESCR] [nvarchar](30) NOT NULL,
	[JOBCODE] [nvarchar](12) NOT NULL,
	[LOCATION] [nvarchar](10) NOT NULL,
	[ORIG_HIRE_DT] [datetime] NULL,
	[OPRID] [nvarchar](30) NULL,
	[SUPERVISOR_ID] [nvarchar](11) NOT NULL,
	[WH_FNCT_MNGR_ID] [nvarchar](11) NOT NULL,
	[SUPV_LVL_ID] [nvarchar](8) NOT NULL,
	[CONTRACT_NUM] [nvarchar](25) NOT NULL,
	[WH_NUM_CNT] [nvarchar](20) NOT NULL,
	[WH_NUM_AVENANT] [nvarchar](20) NOT NULL,
	[WH_STAFFING_ID] [nvarchar](10) NOT NULL,
	[DEPTID] [varchar](500) NULL,
 CONSTRAINT [PK_pers_max_groupe2] PRIMARY KEY CLUSTERED 
(
	[EMPLID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SESSION]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SESSION](
	[SESS_ID] [int] IDENTITY(1,1) NOT NULL,
	[Code de la session] [varchar](255) NULL,
	[SESS_DATE_BEGIN] [date] NULL,
	[SESS_DATE_END] [date] NULL,
	[THEME] [nvarchar](max) NULL,
	[TRAINING_TYPE] [nvarchar](max) NULL,
	[TRAINER_MATRICULE] [nvarchar](50) NULL,
	[TRAINER_LAST_NAME] [nvarchar](max) NULL,
	[TRAINER_FIRST_NAME] [nvarchar](max) NULL,
	[CREATE_DATE] [date] NULL,
	[Code de l'action] [nvarchar](50) NULL,
	[Nom de l'action] [nvarchar](50) NULL,
	[statut_session] [nchar](10) NULL,
 CONSTRAINT [PK_SESSION] PRIMARY KEY CLUSTERED 
(
	[SESS_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Settings]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Settings](
	[application] [int] NOT NULL,
	[key] [varchar](50) NOT NULL,
	[value] [varchar](256) NULL,
	[omit] [bit] NULL,
 CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
(
	[application] ASC,
	[key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SOURCE_TALENT]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SOURCE_TALENT](
	[Key] [varchar](255) NOT NULL,
	[Source ID] [int] IDENTITY(1,1) NOT NULL,
	[Code de l'action] [varchar](255) NULL,
	[Nom de l'action] [varchar](50) NULL,
	[Theme] [varchar](50) NULL,
	[Code de la session] [varchar](255) NULL,
	[Nom formateur de la session] [varchar](50) NULL,
	[Prénom formateur de la session] [varchar](50) NULL,
	[Login Formateur] [varchar](50) NULL,
	[Matricule formateur] [varchar](50) NULL,
	[Type de formation] [varchar](50) NULL,
	[Date début de la session] [date] NULL,
	[Date fin de la session] [date] NULL,
	[Statut de la session] [varchar](50) NULL,
	[Durée de la sous session] [varchar](50) NULL,
	[Date Début Sous Session] [date] NULL,
	[Date Fin Sous Session] [date] NULL,
	[Matricule] [varchar](50) NULL,
	[Nom] [varchar](50) NULL,
	[Prénom] [varchar](50) NULL,
	[Login] [varchar](50) NULL,
	[PPC Campagny] [varchar](50) NULL,
	[DESC PPC Campagny] [varchar](50) NULL,
	[PPC Webhelp Group] [varchar](50) NULL,
	[Nom Référentiel métier] [varchar](50) NULL,
	[Code Référentiel métier] [varchar](50) NULL,
	[Etat de l’inscription] [varchar](50) NULL,
	[Code Référentiel métier niveau 1] [varchar](255) NULL,
	[Code Référentiel métier niveau 2] [varchar](255) NULL,
	[Code Référentiel métier niveau 3] [varchar](255) NULL,
	[PPC Location] [varchar](255) NULL,
	[DESC PPC Location] [varchar](255) NULL,
	[Pays] [varchar](50) NULL,
	[Training Catalogue niveau 1] [varchar](255) NULL,
	[DESC Training Catalogue niveau 1] [varchar](255) NULL,
	[Training Catalogue niveau 2] [varchar](255) NULL,
	[DESC Training Catalogue niveau 2] [varchar](255) NULL,
	[Training Catalogue niveau 3] [varchar](255) NULL,
	[DESC Training Catalogue niveau 3] [varchar](255) NULL,
	[Training Catalogue niveau 4] [varchar](255) NULL,
	[DESC Training Catalogue niveau 4] [varchar](255) NULL,
	[UPDATE_DATE] [date] NULL,
	[idFile] [int] NULL,
 CONSTRAINT [PK_SOURCE_TALENT] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SUB_SESSION]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SUB_SESSION](
	[SUBS_ID] [int] IDENTITY(1,1) NOT NULL,
	[SUBS_DATE] [date] NULL,
	[SUBS_DURATION] [varchar](50) NULL,
	[SESS_ID] [varchar](255) NULL,
 CONSTRAINT [PK_SUB_SESSION] PRIMARY KEY CLUSTERED 
(
	[SUBS_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TEMPORARY_TALENT]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TEMPORARY_TALENT](
	[Key] [nvarchar](max) NULL,
	[Code de l'action] [nvarchar](max) NULL,
	[Nom de l'action] [nvarchar](max) NULL,
	[theme] [nvarchar](max) NULL,
	[Code de la session] [nvarchar](max) NULL,
	[Nom formateur de la session] [nvarchar](max) NULL,
	[Prénom formateur de la session] [nvarchar](max) NULL,
	[Login Formateur] [nvarchar](max) NULL,
	[Matricule formateur] [nvarchar](max) NULL,
	[type de formation] [nvarchar](max) NULL,
	[Date début de la session] [nvarchar](max) NULL,
	[Date fin de la session] [nvarchar](max) NULL,
	[Statut de la session] [nvarchar](max) NULL,
	[Durée de la sous session] [nchar](10) NULL,
	[Date Début Sous Session] [nvarchar](max) NULL,
	[Date Fin Sous Session] [nvarchar](max) NULL,
	[Matricule] [nvarchar](max) NULL,
	[Nom] [nvarchar](max) NULL,
	[Prénom] [nvarchar](max) NOT NULL,
	[Login] [nvarchar](max) NULL,
	[PPC Campagny] [nvarchar](max) NULL,
	[DESC PPC Campagny] [nvarchar](max) NULL,
	[PPC Webhelp Group] [nvarchar](max) NULL,
	[Nom Référentiel métier] [nvarchar](max) NULL,
	[Code Référentiel métier] [nvarchar](max) NULL,
	[Etat de l’inscription] [nvarchar](max) NULL,
	[Code Référentiel métier niveau 1] [nvarchar](max) NULL,
	[Code Référentiel métier niveau 2] [nvarchar](max) NULL,
	[Code Référentiel métier niveau 3] [nvarchar](max) NULL,
	[PPC Location] [nvarchar](max) NULL,
	[DESC PPC Location] [nvarchar](max) NULL,
	[Pays] [nvarchar](max) NULL,
	[Training Catalogue niveau 1] [nvarchar](max) NULL,
	[DESC Training Catalogue niveau 1] [nvarchar](max) NULL,
	[Training Catalogue niveau 2] [nvarchar](max) NULL,
	[DESC Training Catalogue niveau 2] [nvarchar](max) NULL,
	[Training Catalogue niveau 3] [nvarchar](max) NULL,
	[DESC Training Catalogue niveau 3] [nvarchar](max) NULL,
	[Training Catalogue niveau 4] [nvarchar](max) NULL,
	[DESC Training Catalogue niveau 4] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TRAINEE]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TRAINEE](
	[Key] [varchar](255) NOT NULL,
	[Réalisées] [int] NULL,
	[SUBS_V_STATUS_EMP] [bit] NULL CONSTRAINT [DF_TRAINEE_SUBS_V_STATUS_EMP]  DEFAULT ((0)),
	[EMPLID] [nvarchar](11) NULL,
	[ctime] [datetime] NULL CONSTRAINT [DF_TRAINEE_ctime]  DEFAULT (getdate()),
	[mtime] [datetime] NULL,
 CONSTRAINT [PK_TRAINEE] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[ViewTRAINEE]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[ViewTRAINEE]
as SELECT        S.[Key], S.[Code de la session], S.Matricule,S.[DESC Training Catalogue niveau 4] as Projet ,S.[Code Référentiel métier] as [Code emploi] , S.[Nom Référentiel métier] as [DESC du code emploi],S.[PPC Campagny] as Company, S.[DESC PPC Campagny] as [Desc of company], S.Nom + ' ' + S.Prénom AS Stagiaire, S.[Date Début Sous Session] AS Date, S.[Durée de la sous session] AS Prévues,T.SUBS_V_STATUS_EMP AS Statut, T.Réalisées/60 as Réalisées,  T.mtime,
                      S.Pays, S.[DESC PPC Location], S.Theme, S.[Type de formation], S.[Statut de la session], S.[Date début de la session], S.[Date fin de la session], S.[Nom formateur de la session],
                      S.[Prénom formateur de la session], S.[Nom de l'action],
                       (CASE WHEN T.[SUBS_V_STATUS_EMP] = 1 THEN 'Validée' ELSE (CASE WHEN T.[Réalisées]<S.[Durée de la sous session] THEN 'En Cours' ELSE 'Non Validée' END) END) AS [Etat de la session]
FROM            dbo.SOURCE_TALENT AS S LEFT OUTER JOIN
                      dbo.TRAINEE AS T ON S.[Key] = T.[Key]





GO
/****** Object:  View [dbo].[ViewSESSION]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [dbo].[ViewSESSION] as
SELECT        [Nature formation N2], [Statut de la session], [Nature formation N1], ID, Action, [Code de la session], [Type de formation], Projet, Formateur, [Nombre de stagiaire], 
                         (CASE WHEN [SS Validée] = 0 THEN 'Non Validée' ELSE (CASE WHEN [SS Validée] = [SS Prévues] THEN 'Validée' ELSE 'En Cours' END) END) AS [Etat de la session], (CASE WHEN [SS Validée] = [SS Prévues] THEN 1 ELSE 0 END) 
                         AS [Statut de validation], [Date début de la session], [Date fin de la session], [SS Prévues], [SS Validée]
FROM            (SELECT        [DESC Training Catalogue niveau 2] AS [Nature formation N1], [DESC Training Catalogue niveau 3] AS [Nature formation N2], [Statut de la session], [Code de l'action] AS ID, [Nom de l'action] AS Action, 
                                                    [Code de la session], [Type de formation],  [DESC Training Catalogue niveau 4 ] AS Projet, [Nom formateur de la session] + ' ' + [Prénom formateur de la session] AS Formateur, COUNT(DISTINCT Matricule) 
                                                    AS [Nombre de stagiaire], COUNT([Key]) AS [SS Prévues],
                                                        (SELECT        SUM(CASE WHEN [Statut] = 1 THEN 1 ELSE 0 END) AS Expr1
                                                          FROM            dbo.ViewTRAINEE AS nolock
                                                          WHERE        ([Code de la session] = S.[Code de la session])) AS [SS Validée], MIN([Date début de la session]) AS [Date début de la session], MAX([Date fin de la session]) AS [Date fin de la session]
                          FROM            dbo.SOURCE_TALENT AS S WITH (nolock)
                          GROUP BY [Code de l'action], [Nom de l'action], [Code de la session], [Statut de la session], [Type de formation], [DESC Training Catalogue niveau 4], [DESC Training Catalogue niveau 3 ], [DESC Training Catalogue niveau 2], 
                                                    [Nom formateur de la session], [Prénom formateur de la session]) AS G


GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_SOURCE_TALENT]    Script Date: 14/12/2017 17:08:32 ******/
CREATE NONCLUSTERED INDEX [IX_SOURCE_TALENT] ON [dbo].[SOURCE_TALENT]
(
	[Nom de l'action] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Get]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
[dbo].[Get] 1,'TalentSoft','ip'
*/
CREATE PROCEDURE [dbo].[Get] 
    @Mode smallint,@P1 varchar(256)='',@P2 varchar(256)=''
AS
    declare @id int=0
    
IF @Mode=1 BEGIN
    select S.[value] from [dbo].[Applications] as A, [dbo].[Settings] as S where A.id=S.[application] and A.[name]=@P1 and S.[key]=@P2
END
GO
/****** Object:  StoredProcedure [dbo].[Set]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Set] 
    @Mode smallint,@P1 varchar(256)='',@P2 varchar(256)='',@P3 varchar(256)=''
AS
    declare @id int=0
    
IF @Mode=1 BEGIN
    select @id=id from [dbo].[Files] with (nolock) where [name]=@P1 and [size]=@P2 and [mtime]=@P3
    if @@rowcount=0 begin
        INSERT INTO [dbo].[Files] ([name],[size],[mtime]) VALUES (@P1,@P2,@P3)
        select @id=@@identity
    end
    select @id as id
END

CREATE TABLE [dbo].[Settings](
    [application] [varchar](50) NOT NULL,
    [key] [varchar](50) NOT NULL,
    [value] [varchar](256) NULL,
    [omit] [bit] NULL,
CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
(
    [application] ASC,
    [key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  StoredProcedure [dbo].[toSESSION]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[toSESSION]
AS
BEGIN
	insert into [dbo].[SESSION]([Code de la session],[SESS_DATE_BEGIN],
	[SESS_DATE_END],[THEME],[TRAINING_TYPE],[TRAINER_MATRICULE],
	[TRAINER_LAST_NAME],[TRAINER_FIRST_NAME],[CREATE_DATE],
	[Code de l'action],[Nom de l'action],[statut_session])
	select[Code de la session] ,[Date début de la session],[Date fin de la session],[Theme],
	[Type de formation],[Matricule formateur],[Nom formateur de la session],
	[Prénom formateur de la session],GETDATE(),[Code de l'action],[Nom de l'action],[Statut de la session] 
	from [dbo].[SOURCE_TALENT] where [Code de la session] not in (select [Code de la session] from [dbo].[SESSION])  GROUP BY [Code de l'action], [Nom de l'action], [Code de la session],
	[Date début de la session],[Date fin de la session],[Theme],
		[Type de formation],[Matricule formateur],[Nom formateur de la session],
			[Prénom formateur de la session],[Code de l'action],[Nom de l'action],[Statut de la session] 

END
GO
/****** Object:  StoredProcedure [dbo].[toSOURCE]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
delete from  dbo.TEMPORARY_TALENT
delete from  [dbo].[SOURCE_TALENT] 
exec [dbo].[toSOURCE]

select * from  dbo.TEMPORARY_TALENT
*/

CREATE PROCEDURE [dbo].[toSOURCE] @ID int, @File varchar(256)=''
AS
BEGIN
declare @lines int
declare @query varchar(max)

delete from dbo.TEMPORARY_TALENT

set @query = 'BULK INSERT dbo.TEMPORARY_TALENT FROM '''+@File+'''	WITH (CODEPAGE = ''ACP'', FIELDTERMINATOR = '';'',	ROWTERMINATOR = ''\n'',	FIRSTROW = 2);'
execute(@query)	

set @lines = @@ROWCOUNT

/*
	BULK INSERT dbo.TEMPORARY_TALENT
	FROM 'C:\temp\TS_EXPORT_FACTURATION_2017112923.csv'
	WITH (
	CODEPAGE = 'ACP', 
	FIELDTERMINATOR = ';',
	ROWTERMINATOR = '\n',
	FIRSTROW = 2
	);
*/
INSERT INTO [dbo].[SOURCE_TALENT] 
SELECT [Key]
     ,[Code de l'action]
     ,[Nom de l'action]
     ,[theme]
     ,[Code de la session]
     ,[Nom formateur de la session]
     ,[Prénom formateur de la session]
     ,[Login Formateur]
     ,[Matricule formateur]
     ,[type de formation]
     ,convert(date,[Date début de la session],103)
     ,convert(date,[Date fin de la session],103)
     ,[Statut de la session]
     ,[Durée de la sous session]
     ,convert(date,[Date Début Sous Session],103)
     ,convert(date,[Date Fin Sous Session],103)
     ,[Matricule]
     ,[Nom]
     ,[Prénom]
     ,[Login]
     ,[PPC Campagny]
     ,[DESC PPC Campagny]
     ,[PPC Webhelp Group]
     ,[Nom Référentiel métier]
     ,[Code Référentiel métier]
     ,[Etat de l’inscription]
     ,[Code Référentiel métier niveau 1]
     ,[Code Référentiel métier niveau 2]
     ,[Code Référentiel métier niveau 3]
     ,[PPC Location]
     ,[DESC PPC Location]
     ,[Pays]
     ,[Training Catalogue niveau 1]
     ,[DESC Training Catalogue niveau 1]
     ,[Training Catalogue niveau 2]
     ,[DESC Training Catalogue niveau 2]
     ,[Training Catalogue niveau 3]
     ,[DESC Training Catalogue niveau 3]
     ,[Training Catalogue niveau 4]
     ,[DESC Training Catalogue niveau 4]
      ,getdate(),@ID
 FROM [dbo].[TEMPORARY_TALENT] as T 
    where NOT EXISTS
   (SELECT * FROM [dbo].[SOURCE_TALENT] S
   WHERE S.[Key] = T.[Key])

	exec[dbo].[toTRAINEE]
	EXEC [dbo].[toSESSION]

	EXEC [dbo].[toSUBSESSION]

	select @lines 
END

GO
/****** Object:  StoredProcedure [dbo].[toSUBSESSION]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[toSUBSESSION]
AS
BEGIN
	insert into [dbo].[SUB_SESSION]([SUBS_DATE],[SUBS_DURATION],[SESS_ID])
	select A.[Date Début Sous Session],A.[Durée de la sous session],B.[Code de la session]
	from [dbo].[SOURCE_TALENT]as A LEFT  OUTER JOIN [dbo].[SESSION] as B  ON B.[Code de la session]=A.[Code de la session] 
	where A.[Durée de la sous session] + convert(varchar(10), A.[Date Début Sous Session]) not in (select [SUBS_DURATION]+convert(varchar(10),[SUBS_DATE]) from [dbo].[SUB_SESSION])
	group by A.[Date Début Sous Session],A.[Durée de la sous session],B.[Code de la session]
END

GO
/****** Object:  StoredProcedure [dbo].[toTRAINEE]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[toTRAINEE]
AS
BEGIN
	insert into [dbo].[TRAINEE]([Key],[Réalisées])
	select [Key],[Durée de la sous session] from [dbo].[SOURCE_TALENT] where [Key] not in (select [Key] from [dbo].[TRAINEE])

END

GO
/****** Object:  StoredProcedure [dbo].[ValidationSousSession]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ValidationSousSession] @P1 varchar(255), @P2 varchar(50), @P3 varchar(255),@P4 varchar(255),@P5 varchar(255)
AS
BEGIN

declare @S table ([Key] varchar(255),[Date Début Sous Session] date)

insert into @S
select [Key],[Date Début Sous Session]
from  [dbo].[SOURCE_TALENT] 
where [Code de la session]=@P1 
and [Nom de l'action]=@P2 
and ([Nom formateur de la session]+' '+[Prénom formateur de la session])=@P3 
and [DESC Training Catalogue niveau 3]=@P4 
and [DESC Training Catalogue niveau 2]=@P5


select [Date Début Sous Session],(case when [Nombre Stagiaire]=[Nombre Stagiaire Validé] then 1 else 0 end) as [Statut de validation],[Nombre Stagiaire],[Nombre Stagiaire Validé]
from (
select  [Date Début Sous Session], 
count(0) as [Nombre Stagiaire],
(select sum(case when [SUBS_V_STATUS_EMP]=1 then 1 else 0 end) from [dbo].[TRAINEE] nolock where [Key] in (select [Key] from @S where [Date Début Sous Session]=S.[Date Début Sous Session])) as [Nombre Stagiaire Validé]
from @S as S
group by [Date Début Sous Session]
) as G
order by [Date Début Sous Session] asc


END
GO
/****** Object:  StoredProcedure [dbo].[ValidationStagiaire]    Script Date: 14/12/2017 17:08:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ValidationStagiaire] @P1 varchar(255), @P2 varchar(50), @P3 varchar(255), @P4 varchar(50),@P5 varchar(50),@P6 varchar(50)
AS
BEGIN

select S.[Key],S.Matricule, (S.Nom+' '+S.Prénom) as Stagiaire,S.[Date Début Sous Session] as Date, S.[Durée de la sous session] as Prévues,T.Réalisées,T.SUBS_V_STATUS_EMP as Statut,T.mtime
from dbo.SOURCE_TALENT as S LEFT OUTER JOIN [dbo].[TRAINEE] as T 
ON S.[Key]=T.[Key]
where [Code de la session]=@P1 and [Nom de l'action]=@P2 and ([Nom formateur de la session]+' '+[Prénom formateur de la session])=@P3
and [Date Début Sous Session]=@P4 and [DESC Training Catalogue niveau 3]=@P5 
and [DESC Training Catalogue niveau 2]=@P6

END

GO
USE [master]
GO
ALTER DATABASE [TalentSoftv0.1] SET  READ_WRITE 
GO
